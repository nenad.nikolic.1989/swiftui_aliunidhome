//
//  Strings.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/17/24.
//

import Foundation

enum Strings: String{
    case login
    case logout
    case retry
    case register
    case your_email_address
    case password
    case history
    case no_chart_data
    case my_utility
    case my_energy
    case my_el_meter
    case shop
    case my_orders
    case profile_and_settings
}

extension Strings {
    var localized: String {
        return rawValue.localization()
    }
}


