//
//  Constant.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/25/24.
//

import Foundation

struct StorageKeys {
    static let USER_AUTHENTIFICATION = "user_authentification"
}

struct NetworkConstants {
    
    static let BASE_SERVER_URL = "https://test.api.aliunid.com/api"
    static let VERSION = "/v5"
    static let ME = "/me"
    
    static let SIGN_IN = "/signin"
    static let PROFILE = "/profile"
    static let UTILITY = "/utility"
}


enum Authorization {
    static let CLIENT_ID = "siot-mobile"
    static let APP_AUTH_KEY = "authState"
#if DEBUG
    static let AUTH_ENPOINT = "https://api-test.siot.cloud"
    static let REDIRECT_URI = "com.appmodule.siotclient"
#else
    static let AUTH_ENPOINT = "https://iam.aliunid.com/auth/realms/SIOT"
    static let REDIRECT_URI = "com.appmodule.siotapp://auth/*"
#endif
}
