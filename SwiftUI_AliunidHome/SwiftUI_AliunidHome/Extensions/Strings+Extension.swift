//
//  Strings+Extension.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/17/24.
//

import Foundation

// MARK: String extension for localization
extension String {
    func localization() -> String {
        guard let bundle = Bundle.main.path(forResource: LanguageManager.shared.currentLanguage.rawValue, ofType: "lproj") else {
            return NSLocalizedString(self, comment: "")
        }
        
        let langBundle = Bundle(path: bundle)
        return NSLocalizedString(self, tableName: nil, bundle: langBundle!, comment: "")
    }
}
