//
//  Notification+Extension.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/25/24.
//

import Foundation

extension Notification.Name {
    static let userAuthenticationExpired = Notification.Name("UserAuthenticationExpired")
}
