//
//  UserDefaultManager.swift
//  Siot
//
//  Created by Nenad Nikolic on 5/31/23.
//

import AppAuth
import SwiftUI

class UserDefaultsManager {
    
    enum Key: String {
        case token
        case refress_token
        case isUserLogged
    }
    
    static let shared: UserDefaultsManager = .init()
    
    @AppStorage(Key.isUserLogged.rawValue) var isLogged: Bool = false

    func setToken(token: Token) {
           do {
               let encodedData = try JSONEncoder().encode(token)
               UserDefaults.standard.set(encodedData, forKey: Key.token.rawValue)
           } catch {
               print("Error saving token: \(error.localizedDescription)")
           }
       }

       func getToken() -> Token? {
           if let tokenData = UserDefaults.standard.data(forKey: Key.token.rawValue) {
               do {
                   let token = try JSONDecoder().decode(Token.self, from: tokenData)
                   return token
               } catch {
                   print("Error decoding token: \(error.localizedDescription)")
                   return nil
               }
           }
           return nil
       }
}

extension UserDefaultsManager {
    func setValue<T>(key: Key, value: T) {
        UserDefaults.standard.set(value, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    func getValue<T>(key: Key) -> T? {
        UserDefaults.standard.object(forKey: key.rawValue) as? T
    }
    
    func deleteValue(key: Key) {
        UserDefaults.standard.removeObject(forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
}
