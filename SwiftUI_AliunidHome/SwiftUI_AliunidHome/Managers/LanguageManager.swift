//
//  LanguageManager.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/28/24.
//

import SwiftUI

import SwiftUI

class LanguageManager: ObservableObject {
    static let shared: LanguageManager = LanguageManager()
    
    @Published var currentLanguage: Languages {
        didSet {
            UserDefaults.standard.set(currentLanguage.rawValue, forKey: "selectedLanguage")
            setLanguage(language: currentLanguage)
        }
    }
    
    var appLocale: Locale {
        get {
            return Locale(identifier: currentLanguage.rawValue)
        }
    }
    
    private init() {
        if let savedLanguage = UserDefaults.standard.string(forKey: "selectedLanguage"),
           let language = Languages(rawValue: savedLanguage) {
            self.currentLanguage = language
        } else {
            self.currentLanguage = .english
        }
    }
    
    func setLanguage(language: Languages) {
        UserDefaults.standard.set([language.rawValue], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
    }
}


enum Languages: String {
    case german = "de"
    case english = "en"
}
