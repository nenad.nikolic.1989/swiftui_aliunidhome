//
//  NetworkAlamofireManager.swift
//  Siot
//
//  Created by Nenad Nikolic on 6/1/23.
//

import Alamofire
import AppAuth
import Foundation

public class NetworkManager {
    private var retryLimit = 1
    private let timeout = 15.0
    private var userDefaultManager = UserDefaultsManager.shared
    static let shared = NetworkManager()
    
    // TODO: - ADD BASE URL
    private let API_BASE_URL = NetworkConstants.BASE_SERVER_URL
    private let refreshTokenUrl = "https://test.api.aliunid.com/api/v5/signin" 
    private var alamofireManager: Session!
    
    private init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 15
        configuration.timeoutIntervalForRequest = 15
        self.alamofireManager = Alamofire.Session(configuration: configuration)
    }
    
    func runRequestWith<T: Decodable>(path: String, method: HTTPMethod = .get, parameters: Parameters? = nil, headers: HTTPHeaders? = nil) async throws -> T? {
        return try await withCheckedThrowingContinuation { continuation in
            alamofireManager.request(API_BASE_URL + path ,
                                     method: method,
                                     parameters: parameters,
                                     headers: headers,
                                     interceptor: self).validate().responseData { response in
                
                print(response.response?.url?.absoluteString ?? "")
                print("RESPONSE CODE: \(response.response?.statusCode ?? 0)\n")
                switch response.result {
                case let .success(data):
                    continuation.resume(returning: self.handleData(data: data))
                case let .failure(error):
                    continuation.resume(throwing: self.handleError(error: error))
                }
            }
        }
    }
    
}

extension NetworkManager {
    
    func handleData<T: Decodable>(data: Data) -> T? {
        do {
            let parsedData: T = try parseData(data: data)
            return parsedData
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func parseData<T: Decodable>(data: Data) throws -> T {
        guard let decodedData = try? JSONDecoder().decode(T.self, from: data)
        else {
            throw NSError(
                domain: "NetworkAPIError",
                code: 3,
                userInfo: [NSLocalizedDescriptionKey: "Something went wrong."]
            )
        }
        return decodedData
    }
    
    // MARK: - TEMPORARY USING THIS ERROR HANDLING
    func handleError(error: AFError) -> Error {
        if let underlyingError = error.underlyingError {
            let nserror = underlyingError as NSError
            let code = nserror.code
            if code == NSURLErrorNotConnectedToInternet ||
                code == NSURLErrorTimedOut ||
                code == NSURLErrorInternationalRoamingOff ||
                code == NSURLErrorDataNotAllowed ||
                code == NSURLErrorCannotFindHost ||
                code == NSURLErrorCannotConnectToHost ||
                code == NSURLErrorNetworkConnectionLost
            {
                var userInfo = nserror.userInfo
                userInfo[NSLocalizedDescriptionKey] = "Unable to connect to the server"
                let currentError = NSError(
                    domain: nserror.domain,
                    code: code,
                    userInfo: userInfo
                )
                return currentError
            }
        }
        return error
    }
}

// MARK: RequestInterceptor delegate methods

extension NetworkManager: RequestInterceptor {
    
    public func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        var request = urlRequest
        guard let token: String = userDefaultManager.getToken()?.access_token else {
            completion(.success(urlRequest))
            return
        }
        let bearerToken = "Bearer \(token)"
        request.setValue(bearerToken, forHTTPHeaderField: "Authorization")
        print("Token is added to the header field")
        completion(.success(request))
    }
    
    public func retry(_ request: Request, for session: Session, dueTo error: Error,
                      completion: @escaping (RetryResult) -> Void) {
        
        if 401 ... 403 ~= request.response?.statusCode ?? 0 {
            guard request.retryCount < retryLimit else {
                completion(.doNotRetry)
                return
            }
            
            print("STATUS CODE IN RETRY: \(request.response?.statusCode ?? 0)")
            refreshToken { isSuccess in
                if isSuccess {
                    completion(.retry)
                }else {
                    NotificationCenter.default.post(name: Notification.Name.userAuthenticationExpired, object: nil)
                    completion(.doNotRetry)
                }
            }
        }else {
            completion(.doNotRetry)
            return
        }
    }
    
    func refreshToken(completion: @escaping (Bool) -> Void) {
        
        let parameters: [String: Any] = [
            "client_id": "ios",
            "grant_type": "refresh_token",
            "refresh_token": userDefaultManager.getToken()?.refresh_token ?? ""
        ]
        alamofireManager?.request(refreshTokenUrl, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .responseDecodable(of: Token.self) { response in
                switch response.result {
                case .success(let token):
                    if let encodedValue = try? JSONEncoder().encode(token) {
                        self.userDefaultManager.setValue(key: .token, value: encodedValue)
                    }
                    print("TOKEN IS REFRESHED \n")
                    completion(true)
                case .failure:
                    completion(false)
                }
            }
    }
}
