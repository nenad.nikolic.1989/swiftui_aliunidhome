//
//  CockpitView.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/16/24.
//

import SwiftUI

struct CockpitView: View {
    
    @AppStorage(StorageKeys.USER_AUTHENTIFICATION) var isUserLogged = false
    @EnvironmentObject private var cockpitCoordinator: CockpitCoordinator
    @StateObject var viewModel : CockpitViewModel
    @State private var sideMenuOpen: Bool = false

    var body: some View {
        ZStack {
            BackgroundColorView()
            rootView
                .navigationBarBackButtonHidden()
                .navigationBarTitleDisplayMode(.inline)
        }
        .onReceive(NotificationCenter.default.publisher(for: Notification.Name.userAuthenticationExpired)) { _ in
            //MARK: Token is expired, user needs to be logged out!
            isUserLogged = false
        }
        .task {
           // await viewModel
        }
    }
    
    var rootView: some View {
        contentView
            .slideInView(isActive: $sideMenuOpen, options: SlideInViewOptions(paddingColorOpacity: 0.4, shouldDismissUponExternalTap: true)) {
                MenuView(isOpen: $sideMenuOpen, logout: {
                    isUserLogged = false
                })
            }
    }
    
    var contentView: some View {
        VStack {
            navigationButtonsView
            Spacer()
            Text("Cockpit Screen")
            
            Button(action: {
                isUserLogged = false
            }, label: {
                Text("Logout")
            })
            
            Spacer()
        }
    }
    
    var navigationButtonsView: some View {
        HStack {
            ButtonWithImageView(imageName: "menu") {
                self.sideMenuOpen.toggle()
            }
            Spacer()
            ButtonWithImageView(imageName: "notification") {}
        }
        .padding(20)
    }
  
}

#Preview {
    CockpitView(viewModel: CockpitViewModel(cockpitRepo: CockpitWebRepository(networkManager: NetworkManager.shared)))
}
