//
//  Profile.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/26/24.
//

import Foundation


struct ProfileData: Codable, Equatable {
    var id : String?
    var gender : String?
    var firstName : String?
    var lastName : String?
    var email : String?
    var language : String?
    var utilityId : String?
    var zipCode : String?
    var standardProfile : String?
    var sharedData : Bool?
    var termsOfConditions : Bool?
    var privacyCrashlytics : Bool?
    var hasBlinker : Bool?

    public static func ==(lhs: ProfileData, rhs: ProfileData) -> Bool{
        return
            lhs.id == rhs.id &&
            lhs.gender == rhs.gender &&
            lhs.firstName == rhs.firstName &&
            lhs.id == rhs.id &&
            lhs.lastName == rhs.lastName &&
            lhs.email == rhs.email &&
            lhs.language == rhs.language &&
            lhs.utilityId == rhs.utilityId &&
            lhs.zipCode == rhs.zipCode &&
            lhs.sharedData == rhs.sharedData &&
            lhs.termsOfConditions == rhs.termsOfConditions &&
            lhs.privacyCrashlytics == rhs.privacyCrashlytics
    }
}
