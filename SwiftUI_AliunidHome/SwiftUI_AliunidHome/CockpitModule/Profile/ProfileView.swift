//
//  ProfileView.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/19/24.
//

import SwiftUI

struct ProfileView: View {
    
    var body: some View {
        ZStack {
            BackgroundColorView()
            rootView
        }
    }
    
    var rootView: some View {
        contentView
    }
    
    var contentView: some View {
        Text("Profile Screen")
    }
}

#Preview {
    ProfileView()
}
