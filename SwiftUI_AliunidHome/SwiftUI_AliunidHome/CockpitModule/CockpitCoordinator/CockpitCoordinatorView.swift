//
//  CockpitCoordinatorView.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/19/24.
//

import SwiftUI

struct CockpitCoordinatorView: View {
    
    @EnvironmentObject private var cockpitCoordinator: CockpitCoordinator

    var body: some View {
        NavigationStack(path: $cockpitCoordinator.path) {
            cockpitCoordinator.build(view: .cockpit)
                .navigationDestination(for: CockpitViews.self) { view in
                    cockpitCoordinator.build(view: view)
                }
        }
    }
}

#Preview {
    CockpitCoordinatorView()
}
