//
//  CockpitCoordinator.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/19/24.
//

import SwiftUI


enum CockpitViews: String {
    case cockpit, energy, utility, electricityMeter, shop, order, profile
}

class CockpitCoordinator: ObservableObject {
    
    @Published var path = NavigationPath()
    var cockpitRepository = CockpitWebRepository(networkManager: NetworkManager.shared)

    @ViewBuilder
    func build(view: CockpitViews) -> some View {
        switch view {
        case .profile:
            ProfileView()
        case .utility:
            UtilityView(viewModel: UtilityViewModel(cockpitRepo: self.cockpitRepository))
        case .cockpit:
            CockpitView(viewModel: CockpitViewModel(cockpitRepo: self.cockpitRepository))
        case .electricityMeter:
            ElectricityMeterView()
        case .energy:
            EmptyView()
        case .shop:
            EmptyView()
        case .order:
            EmptyView()
        }
    }
  
    func push(_ view: CockpitViews) {
        path.append(view)
    }
    
    func pop() {
        path.removeLast()
    }
    
    func popToRoot() {
        path.removeLast(path.count)
    }
    
}
