//
//  CockpitRepository.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/19/24.


import Foundation
import Alamofire

protocol CockpitRepository {
    func getUtlityData() async throws -> Utility?
    func getProfileData() async throws -> ProfileData?
}

class CockpitWebRepository: CockpitRepository {

    let utilityPath = NetworkConstants.VERSION + NetworkConstants.ME + NetworkConstants.UTILITY
    let profilePath = NetworkConstants.VERSION + NetworkConstants.ME + NetworkConstants.PROFILE

    var networkManager: NetworkManager
    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
   
    func getUtlityData() async throws -> Utility? {
        do {
            let data : Utility? = try await networkManager.runRequestWith(path: utilityPath)
            return data
        }catch {
            throw error
        }
    }
    
    func getProfileData() async throws -> ProfileData? {
        do {
            let data : ProfileData? = try await networkManager.runRequestWith(path: profilePath)
            return data
        }catch {
            throw error
        }
    }
    
}

class MockCockpitWebRepository: CockpitRepository {
    func getUtlityData() async throws -> Utility? { return nil }
    func getProfileData() async throws -> ProfileData? { return nil }
}

