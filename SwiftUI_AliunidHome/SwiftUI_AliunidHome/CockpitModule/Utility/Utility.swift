//
//  Utility.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/25/24.
//

import Foundation

struct Utility : Codable {
    var utilityId : Int?
    var name : String?
    var isPartner : Bool?
    var shortName : String?
    var logos : Logos?
    var contacts : Contacts?
    var zipCodes: [String] = []
}

struct Contacts : Codable {
    var address : String?
    var phoneNumber : String?
    var email : String?
    var website : String?
}

struct Logos: Codable {
    var light : String?
    var dark : String?
}
