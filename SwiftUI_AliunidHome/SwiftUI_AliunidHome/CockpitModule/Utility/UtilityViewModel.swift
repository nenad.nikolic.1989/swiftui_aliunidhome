//
//  UtilityViewModel.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/22/24.
//

import SwiftUI


class UtilityViewModel: ObservableObject {
    
    var cockpitRepo: CockpitRepository
    @Published var utilityData: Utility?
    @Published var imageUrl : String = ""
    
    init(cockpitRepo: CockpitRepository) {
        self.cockpitRepo = cockpitRepo
    }
    
    func setUI(utility: Utility) {
        self.utilityData = utility
    }
    
    func getImageDepenedOnAppearance(scheme: ColorScheme) -> String {
        DispatchQueue.main.async { [self] in
            imageUrl = (scheme == .dark ? utilityData?.logos?.dark : utilityData?.logos?.light) ?? ""
        }
        return imageUrl
    }
    
}


// MARK: - API REQUESTS

extension UtilityViewModel {
    
    @MainActor
    func getUtilityData() async {
       // self.isLoading = true
        do {
            guard let utilityData = try await cockpitRepo.getUtlityData() else {
                return
            }
            setUI(utility: utilityData)
        } catch let error {
            print(error.localizedDescription)
            //self.showErrorToastMessage(error.localizedDescription)
        }
       // return false
    }
    
}
