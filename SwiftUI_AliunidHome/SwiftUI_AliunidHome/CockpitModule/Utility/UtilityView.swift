//
//  UtilityView.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/19/24.
//

import SwiftUI

struct UtilityView: View {
    
    @Environment(\.colorScheme) var colorScheme
    @EnvironmentObject private var cockpitCoordinator: CockpitCoordinator
    @StateObject var viewModel: UtilityViewModel
    
    var body: some View {
        ZStack {
            BackgroundColorView()
            rootView
                .navigationTitle(Strings.my_utility.localized)
                .navigationBarTitleDisplayMode(.inline)
        }
        .task {
            await viewModel.getUtilityData()
        }
    }
    
    var rootView: some View {
        contentView
    }
    
    var contentView: some View {
        VStack {
            AsyncImage(url: URL(string: viewModel.getImageDepenedOnAppearance(scheme: colorScheme))) { image in
                image.resizable()
            } placeholder: {
                ProgressView()
            }
            .aspectRatio(contentMode: .fit)
            .frame(width: 250, height: 150)
            
            
            Button(action: {
                LanguageManager.shared.currentLanguage = .german
            }) {
                Text("Change to German")
            }
            .padding()
            Button(action: {
                LanguageManager.shared.currentLanguage = .english
            }) {
                Text("Change to English")
            }
            .padding()
            

        }
    }
}

#Preview {
    UtilityView(viewModel: UtilityViewModel(cockpitRepo: CockpitWebRepository(networkManager: NetworkManager.shared)))
}
