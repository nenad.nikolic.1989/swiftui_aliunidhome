//
//  ElectricityMeter.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/21/24.
//

import SwiftUI

struct ElectricityMeterView: View {
    
    var body: some View {
        ZStack {
            BackgroundColorView()
            rootView
        }
    }
    
    var rootView: some View {
        contentView
    }
    
    var contentView: some View {
        Text("Electricity Meter Screen")
    }
    
}

#Preview {
    ElectricityMeterView()
}
