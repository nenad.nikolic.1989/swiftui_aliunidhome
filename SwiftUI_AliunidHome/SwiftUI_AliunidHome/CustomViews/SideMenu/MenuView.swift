//
//  SideMenu.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/21/24.
//

import SwiftUI

struct MenuView: View {
    
    @EnvironmentObject private var cockpitCoordinator: CockpitCoordinator
    @Binding var isOpen: Bool
    var logout: () -> Void
    
    var body: some View {
        rootView
    }
    
    var rootView: some View {
        ZStack {
            VStack(alignment: .leading) {
                imageWithPredefinedText
                topSectionView
                Spacer()
            }
            .foregroundColor(.blue)
            .font(.system(size: 15, weight: .medium))
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding([.horizontal, .bottom])
        }
        .background(Color("background"))
    }
    
    var imageWithPredefinedText: some View {
        
        VStack(alignment: .center, spacing: 0) {
            Image("profile_avatar")
                .resizable()
                .scaledToFit()
                .frame(width: 150, height: 100)
            Text("< A good day to do something for the enviroment >")
                .multilineTextAlignment(.center)
                .foregroundColor(.primary)
                .frame(alignment: .center)
                .padding(EdgeInsets(top: 10, leading: 60, bottom: 10, trailing: 60))
        }
        .padding(.top, 20)
    }
    
    var topSectionView: some View {
        VStack(alignment: .leading, spacing: 15) {
            
            MenuItemView(title: "My energy", imageName: "books.vertical") {
                isOpen = false
                cockpitCoordinator.push( .profile)
            }
            MenuItemView(title: "My utility", imageName: "puzzlepiece.extension") {
                isOpen = false
                cockpitCoordinator.push( .utility)
            }
            MenuItemView(title: "My electricity Meter", imageName: "person.2") {
                isOpen = false
                cockpitCoordinator.push(.electricityMeter)
            }
            Spacer().frame(height: 20)
            MenuItemView(title: "Shop", imageName: "person.2") {
                isOpen = false
                cockpitCoordinator.push(.shop)
            }
            MenuItemView(title: "My orders", imageName: "person.2") {
                isOpen = false
                cockpitCoordinator.push(.order)
            }
            MenuItemView(title: "Profile and Settings", imageName: "person.2") {
                isOpen = false
                cockpitCoordinator.push(.profile)
            }
        } 
        .padding(20)
    }
    
}


#Preview {
    MenuView(isOpen: .constant(true), logout: {
        
    })
}
