//
//  MenuItem.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/21/24.
//

import SwiftUI

struct MenuItemView: View {
    
    var title: String
    var imageName: String
    var action: () -> Void
    
    var body: some View {
        Button {
            action()
        } label: {
            HStack(spacing: 15) {
                Image(systemName:imageName)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 25, height: 25)
                    .foregroundColor(Color.primary)
                Text(title)
                    .foregroundColor(Color.primary)
            }
        }
    }
}


struct MenuItemView_Previews: PreviewProvider {
    static var previews: some View {
        MenuItemView(title: "Title", imageName: "circle.fill") { }
    }
}
