//
//  ButtonWithImageView.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/21/24.
//

import SwiftUI

struct ButtonWithImageView: View {
    
    var imageName: String
    var action: () -> Void
    
    var body: some View {
        Button {
            self.action()
        } label: {
            Image(imageName)
                .resizable()
                .renderingMode(.template)
                .foregroundColor(Color.primary)
                .scaledToFit()
                .frame(width: 25, height: 25)
        }
    }
}

#Preview {
    ButtonWithImageView(imageName: "gear", action: {})
}
