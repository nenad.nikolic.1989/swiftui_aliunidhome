//
//  BackgroundColorView.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/21/24.
//

import SwiftUI

struct BackgroundColorView: View {
    
    var body: some View {
        Color("background")
            .ignoresSafeArea()
    }
}

#Preview {
    BackgroundColorView()
}
