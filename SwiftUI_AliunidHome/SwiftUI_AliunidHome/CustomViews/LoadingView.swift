//
//  GlobalLoadingView.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/19/24.
//

import SwiftUI

struct LoadingView: View {
    
    var body: some View {
        
        ZStack {
            Color.black
                .ignoresSafeArea()
                .opacity(0.5)
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle(tint: .black))
                .scaleEffect(1.5)
        }
    }
}

#Preview {
    LoadingView()
        .previewLayout(.sizeThatFits)
}
