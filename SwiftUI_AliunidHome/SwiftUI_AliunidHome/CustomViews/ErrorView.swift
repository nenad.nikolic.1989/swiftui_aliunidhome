//
//  ErrorView.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/19/24.
//

import SwiftUI

struct ErrorView: View {
    let message: String
    @Binding var isShowing: Bool
    
    var body: some View {
        Text(message)
            .padding()
            .foregroundColor(.white)
            .background(Color.black.opacity(0.7))
            .cornerRadius(16)
            .padding(.horizontal)
    }
}

#Preview {
    ErrorView(message: "Error", isShowing: .constant(true))
}
