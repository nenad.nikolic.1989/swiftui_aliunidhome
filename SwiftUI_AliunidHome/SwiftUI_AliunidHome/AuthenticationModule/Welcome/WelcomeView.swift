//
//  WelcomeView.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/14/24.
//

import SwiftUI

struct WelcomeView: View {
    
    @EnvironmentObject private var coordinator: AuthorizationCoordinator
    
    var body: some View {
        NavigationView{
            ZStack {
                Color("background")
                    .ignoresSafeArea()
                rootView
            }
        }
    }
    
    var rootView: some View {
        contentView
            .navigationBarTitle(Text(""), displayMode: .inline)
            .toolbar {
                CustomToolbarImageItem(imageName: "aliunid_small_logo")
            }
    }
    
    var contentView: some View {
        VStack {
            Spacer()
            descriptionView
            Spacer()
            buttonViews
        }
    }
    
    var descriptionView: some View {
        VStack {
            Text("Hello")
                .font(.largeTitle)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.bottom, 10)
            Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ")
                .frame(maxWidth: .infinity, alignment: .leading)
        }
        .padding(20)
    }
    
    var buttonViews: some View {
        VStack {
            Button("Registration") {
                coordinator.push(.registration)
            }
            .buttonStyle(RoundedButtonStyle())
            
            Button("Login") {
                coordinator.push(.login)
            }
            .buttonStyle(RoundedButtonStyle())
        }
    }
}

#Preview {
    WelcomeView()
}
