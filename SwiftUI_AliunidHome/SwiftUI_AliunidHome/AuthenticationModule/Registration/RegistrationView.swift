//
//  RegistrationView.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/14/24.
//

import SwiftUI

struct RegistrationView: View {
    
    @EnvironmentObject private var coordinator: AuthorizationCoordinator
    
    var body: some View {
        rootView
            .toolbar {
                CustomToolbarImageItem(imageName: "aliunid_small_logo")
            }
    }
    
    var rootView: some View {
        Text("Registration")
    }
}

#Preview {
    RegistrationView()
}
