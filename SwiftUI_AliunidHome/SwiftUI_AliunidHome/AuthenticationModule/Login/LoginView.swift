//
//  LoginView.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/14/24.
//

import SwiftUI

struct LoginView: View {
    
    @AppStorage(StorageKeys.USER_AUTHENTIFICATION) var isUserLogged = false
    @EnvironmentObject private var coordinator: AuthorizationCoordinator

    @ObservedObject private var keyboard = KeyboardResponder()
    @StateObject var viewModel: LoginViewModel
    
    @State private var emailIsEditing = false
    @State private var passwordIsEditing = false
    
    var body: some View {
        ZStack {
            Color("background")
                .ignoresSafeArea()
            rootView
            
            if viewModel.isLoading {
                LoadingView()
            }else if viewModel.showError {
                ErrorView(message: viewModel.errorMessage, isShowing: $viewModel.showError)
            }
        }
        .toolbar {
            CustomToolbarImageItem(imageName: "aliunid_small_logo")
            
        }
        .onAppear(perform: {
            viewModel.isLoading = false
        })
    }
    

    var rootView: some View {
        VStack {
            contentView
        }
    }
    
    var contentView: some View {
        GeometryReader { geometry in
            VStack(alignment: .leading, spacing: 10) {
                Spacer().frame(height: geometry.size.height / 5)
                Text("Login")
                    .font(.largeTitle)
                    .padding(.bottom, 20)
                textfieldsView
                Spacer()
                Button("Login") {
                    Task {
                        if await viewModel.login() {
                            self.coordinator.path = NavigationPath()
                            isUserLogged = true
                        }
                    }
                }
                .buttonStyle(RoundedButtonStyle())
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(20)
        }
    }
    
    var textfieldsView: some View {
        VStack(spacing: 20) {
            emailView
            passwordView
        }
        //.padding(.bottom, keyboard.currentHeight)
    }
    
    var emailView: some View {
        VStack{
            Text ("your e-mail adress".uppercased())
                .asterisk()
                .font(.system(size: 12))
                .frame(maxWidth: .infinity, alignment: .leading)
            TextField("email".capitalized, text: $viewModel.email, onEditingChanged: { edit in
                self.emailIsEditing = edit
            })
            .textFieldStyle(ColoredBorder(focused: $emailIsEditing))
        }
    }
    
    var passwordView: some View {
        VStack(alignment: .leading){
            Text ("password".uppercased())
                .asterisk()
                .font(.system(size: 12))
                .frame(alignment: .leading)
            TextField("password".capitalized, text: $viewModel.password, onEditingChanged: { edit in
                self.passwordIsEditing = edit
            })
            .textFieldStyle(ColoredBorder(focused: $passwordIsEditing))
            
        }
    }
    
}

#Preview {
    LoginView(viewModel: LoginViewModel(authRepo: MockAuthenticationWebRepository(), userDefaults: UserDefaultsManager.shared))
}

