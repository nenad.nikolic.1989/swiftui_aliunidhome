//
//  LoginViewModel.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/17/24.
//

import SwiftUI

class LoginViewModel: ObservableObject {
    
    @Published var isLoading: Bool = false
    @Published var showError: Bool = false
    @Published var errorMessage: String = ""
    @Published var email: String = "milosjovac@gmail.com"
    @Published var password: String = "Aaa123456!"
    @Published var isLogedIn = false
    
    var authRepo: AuthenticationRepository
    var userDefaults: UserDefaultsManager
    
    init(authRepo: AuthenticationRepository, userDefaults: UserDefaultsManager) {
        self.authRepo = authRepo
        self.userDefaults = userDefaults
    }
    
    private func showErrorToastMessage(_ message: String) {
        errorMessage = message
        showError = true
        isLoading = false
        print(message)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.showError = false
        }
    }

}


// MARK: - API REQUESTS

extension LoginViewModel {
    
    @MainActor
    func login() async -> Bool {
        self.isLoading = true
        do {
            guard let tokenData: Token = try await authRepo.login(email: email, password: password) else {
                self.showError = false
                self.isLoading = false
                return false
            }
            self.userDefaults.setToken(token: tokenData)
            return true
        } catch let error {
            self.showErrorToastMessage(error.localizedDescription)
        }
        return false
    }
    
}

