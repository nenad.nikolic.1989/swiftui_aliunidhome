//
//  Token.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/19/24.
//

import Foundation

struct Token: Codable {
    var access_token: String
    var refresh_token: String
}
