//
//  MainCoordinator.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/14/24.
//

import SwiftUI

enum Screen: String {
    case welcome, login, registration
}

class AuthorizationCoordinator: ObservableObject {
    
    @Published var path = NavigationPath()
    var authRepository = AuthenticationWebRepository(networkManager: NetworkManager.shared)
    var userDefaults = UserDefaultsManager.shared

    @ViewBuilder
    func build(screen: Screen) -> some View {
        switch screen {
        case .welcome:
            WelcomeView()
        case .login:
            LoginView(viewModel: LoginViewModel(authRepo: self.authRepository, userDefaults: self.userDefaults))
        case .registration:
            RegistrationView()
        }
    }
  
    func push(_ screen: Screen) {
        path.append(screen)
    }
    
    func pop() {
        path.removeLast()
    }
    
    func popToRoot() {
        path.removeLast(path.count)
    }
    
}
