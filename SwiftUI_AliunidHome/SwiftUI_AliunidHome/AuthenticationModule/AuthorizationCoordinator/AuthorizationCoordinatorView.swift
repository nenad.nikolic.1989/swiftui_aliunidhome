//
//  AuthorizationCoordinatorView.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/14/24.
//

import SwiftUI

struct AuthorizationCoordinatorView: View {
    
    @EnvironmentObject var coordinator: AuthorizationCoordinator

    var body: some View {
        NavigationStack(path: $coordinator.path) {
            coordinator.build(screen: .welcome)
                .navigationDestination(for: Screen.self) { screen in
                    coordinator.build(screen: screen)
                }
        }
    }
}

#Preview {
    AuthorizationCoordinatorView()
}
