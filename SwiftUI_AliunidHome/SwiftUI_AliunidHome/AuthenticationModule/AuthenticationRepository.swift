//
//  AuthNetworkRepository.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/17/24.
//

import Foundation
import Alamofire

protocol AuthenticationRepository {
    func registration()
    func login(email: String, password: String) async throws -> Token?
}


class AuthenticationWebRepository: AuthenticationRepository {
 
    let path =  NetworkConstants.VERSION + NetworkConstants.SIGN_IN
    var networkManager: NetworkManager
    
    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    func login(email: String, password: String) async throws -> Token? {
        let parameters = ["username" : email,
                          "password" : password,
                          "client_id": "ios",
                          "grant_type": "password"]
        do {
            let data: Token? = try await networkManager.runRequestWith(path: path, method: .post, parameters: parameters)
            print(data?.access_token ?? "")
            return data
        } catch {
            throw error
        }
    }

    func registration() {
        
    }
}

class MockAuthenticationWebRepository: AuthenticationRepository {
    func login(email: String, password: String) async throws -> Token? {
        return Token(access_token: "", refresh_token: "")
    }
    func registration() {}
}

