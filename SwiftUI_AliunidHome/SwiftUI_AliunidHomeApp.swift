//
//  SwiftUI_AliunidHomeApp.swift
//  SwiftUI_AliunidHome
//
//  Created by Nenad Nikolic on 3/14/24.
//

import SwiftUI

@main
struct SwiftUI_AliunidHomeApp: App {
    
    @AppStorage(StorageKeys.USER_AUTHENTIFICATION) var isUserLogged = false
    @ObservedObject var languageManager = LanguageManager.shared
    
    let authorizationCoordinator = AuthorizationCoordinator()
    let cockpitCoordinator = CockpitCoordinator()
        
    var body: some Scene {
        WindowGroup {
            if isUserLogged {
                CockpitCoordinatorView()
                    .environmentObject(cockpitCoordinator)
            }else {
                AuthorizationCoordinatorView()
                    .environmentObject(authorizationCoordinator)
            }
        }
        .environment(\.locale, languageManager.appLocale)
    }
}
